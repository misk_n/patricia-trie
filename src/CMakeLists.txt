set(PATRICIA_TRIE_SRC
        dynamic_trie.cpp
        static_trie.cpp
        serialize_visitor.cpp
        dot_visitor.cpp
        delete_visitor.cpp
        word_listing_visitor.cpp)

set (COMPILER_SRC
        ${PATRICIA_TRIE_SRC}
        compiler.cpp)

set (APPLICATION_SRC
        ${PATRICIA_TRIE_SRC}
        application.cpp)

set (LOOKUP_SRC
        ${PATRICIA_TRIE_SRC}
        trie_lookup.cpp)

add_executable(TextMiningCompiler ${COMPILER_SRC})
add_executable(TextMiningApp ${APPLICATION_SRC})
add_executable(Lookup ${LOOKUP_SRC})
