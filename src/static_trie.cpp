#include <algorithm>
#include <cstdlib>
#include "static_trie.hpp"
#include "search_result.hpp"

#include <string>
#include <list>

namespace patricia
{

    static_trie::static_trie(const void *base_ptr, unsigned buffer_length)
      : base_ptr_(base_ptr)
      , control_block_((struct control *)base_ptr)
    {
        node_base_ = (struct node*)((char*)base_ptr + control_block_->nodes);
        arc_base_ = (struct arc*)((char*)base_ptr + control_block_->arcs);
        string_base_ = ((char*) base_ptr_ + control_block_->strings);
        buffer_length_ = buffer_length;
    }

    void static_trie::accept(static_visitor &v) const
    {
        v.visit(get_node_at(0));
    }
    

    inline static bool operator< (const static_trie::arc& a, const static_trie::arc& b)
    {
        return a.key < b.key;
    }

    int damerau(const std::vector<char>& word1,
                const std::string& word2,
                vect_value_t * curRow,
                std::vector<vect_value_t *>& distCompute) {
        int i = curRow[0];
        char dist;
        int deleteCost, insertCost, subCost;
        const unsigned len2 = word2.size();
        for (unsigned j = 0; j < len2; j++) {
            dist = word1[word1.size() - 1] != word2[j];
            deleteCost = distCompute[i - 1][j + 1] + 1;
            insertCost = curRow[j] + 1;
            subCost = distCompute[i - 1][j] + dist;
            curRow[j + 1] =  std::min(std::min(deleteCost, insertCost), subCost);
            if (i > 1 && (j + 1) > 1 && word1[i - 2] == word2[j] && word1[i - 1] == word2[j - 1])
                curRow[j + 1] =  std::min(curRow[j + 1], distCompute[i - 2][j - 1] + dist);
        }
        distCompute.push_back(curRow);
        const auto result = std::min_element(curRow, curRow + len2 + 1);
        return *result;
    }

    void clearData(unsigned len, std::vector<char>& c_s,
                   std::vector<vect_value_t *>& distCompute) {
        bool cleared = false;
        if (len == c_s.size()) {
            c_s.clear();
            cleared = true;
        }
        for (unsigned i = 0; i < len; i++) {
            if (!cleared)
                c_s.pop_back();
            delete[] distCompute.back();
            distCompute.pop_back();
        }
    }


    bool static_trie::searchRecursive(const node* n,
                                      const std::string& word,
                                      std::vector<char>& c_s,
                                      std::vector<vect_value_t *>& distCompute,
                                      int maxDist,
                                      std::vector<SearchResult>& s_r) const
    {
        int res = false;
        int err, j;
        if (n->get_terminal()) {
            int sz = distCompute[distCompute.size() - 1][word.size()];
            if (sz <= maxDist) {
                s_r.push_back(SearchResult(n->value, std::string(c_s.begin(), c_s.end()), sz));
                res = true;
            }
        }
        const unsigned n_arcs_len = n->arcs_length;
        for (unsigned i = 0; i < n_arcs_len; i++) {
            const arc a = *get_arc_at(n->arcs_index + i);

            vect_value_t *curRow = new vect_value_t[word.size() + 1];
            curRow[0] = distCompute.size();
            c_s.push_back(a.key);
            int dist = damerau(c_s, word, curRow, distCompute);
            if (dist > maxDist)
            {
                clearData(1, c_s, distCompute);
                continue;
            }

            if (!a.str_length) {
                res = searchRecursive(get_node_at(a.node_index), word, c_s, distCompute, maxDist, s_r);
                clearData(1, c_s, distCompute);
                continue;
            }

            err = 0;
            for (j = 0; j < a.str_length; j++) {
                vect_value_t *curRow = new vect_value_t[word.size() + 1];
                curRow[0] = distCompute.size();
                c_s.push_back(*get_string_at(a.str_index + j));
                int dist = damerau(c_s, word, curRow, distCompute);
                if (dist > maxDist){
                    err = 1;
                    break;
                }
            }

            if (err)
            {
                clearData(j + 2, c_s, distCompute);
                continue;
            }

            res = searchRecursive(get_node_at(a.node_index), word, c_s, distCompute, maxDist, s_r);
            clearData(a.str_length + 1, c_s, distCompute);

        }
        return res;
    }

    void static_trie::search(const std::string& word, int maxDist) const {
        std::vector<SearchResult> results;
        std::vector<char> c_s;
        std::vector<vect_value_t *> distCompute;
        vect_value_t *currRow = new vect_value_t[word.size() + 1];
        for (unsigned i = 0; i < word.size() + 1; i++)
            currRow[i] = i;
        distCompute.push_back(currRow);
        searchRecursive(get_node_at(0), word, c_s, distCompute, maxDist, results);
        std::sort(results.begin(), results.end());
        print_json(results);
        delete[] currRow;
        // results.clear();
    }
    
    void static_trie::print_json(const std::vector<SearchResult>& results) const {
        std::cout << "[";
        unsigned i = 0;
        if (results.size()) {
            for (;i < results.size() - 1; i++){
            results[i].toJson();
                std::cout << ",";
            }
            results[i].toJson();
        }
        std::cout << "]" << std::endl;
    }


    void static_trie::dump(std::ostream &o) const
    {
        unsigned str_pos = control_block_->strings;
        const char *strings = (const char *) base_ptr_ + str_pos;
        o << "Strings: " << std::endl;
        for (auto i = strings; i < (char *) node_base_; i++)
        {
            o << *i;
        }
        o << std::endl << "Nodes:" << std::endl;
        for (auto i = node_base_; i < (struct node *) arc_base_; i++)
        {
            o << "    node:" << std::endl;
            o << "        arcs_index: " << i->arcs_index << std::endl;
            o << "        arcs_length: " << (unsigned)i->arcs_length << std::endl;
            o << "        node value: " << i->value << std::endl;
            o << "        terminal: " << i->get_terminal() << std::endl;
        }
        o << std::endl << "Arcs:" << std::endl;
        for (auto i = control_block_->arcs; i < buffer_length_; i += sizeof(struct arc))
        {
            char *ptr = (char *) base_ptr_ + i;
            struct arc *a = (struct arc *) ptr;
            o << "    arc:" << std::endl;
            o << "        key: " << a->key << std::endl;
            o << "        node index: " << a->node_index << std::endl;
            o << "        str index: " << a->str_index << std::endl;
            o << "        str length: " << a->str_length << std::endl;
        }
    }

    void static_trie::node::accept(static_visitor &v) const
    {
        unsigned base = arcs_index;
        for (unsigned i = 0; i < arcs_length; i++)
            v.visit(v.get_static_trie()->get_arc_at(base + i));
    }

    void static_trie::arc::accept(static_visitor &v) const
    {
        v.visit(v.get_static_trie()->get_node_at(node_index));
    }

    const static_trie *static_visitor::get_static_trie() const
    {
        return static_trie_;
    }
}