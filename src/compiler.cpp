# include <iostream>
# include <fstream>
# include <serialize_visitor.hpp>
# include <sys/mman.h>
# include <fcntl.h>
# include <sys/stat.h>

# include "dynamic_trie.hpp"
# include "dot_visitor.hpp"
# include "trash_talker.hpp"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " [words file] [dict file]" << std::endl;
        exit(1);
    }

    std::ifstream words;
    words.open(argv[1]);
    if (!words.is_open())
    {
        std::cerr << "Unable to open words file: " << argv[1] << std::endl;
        exit(2);
    }

    patricia::dynamic_trie t;
    while (words.good())
    {
        std::string w;
        int value;
        words >> w;
        if (w.size()) {
            words >> value;
            t.insert(w, value);
        }
    }

    {
        auto v = patricia::serialize_visitor(argv[2]);
        v.visit(&t);
    }

    return 0;
}
