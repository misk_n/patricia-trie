# include <sys/mman.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

#include "serialize_visitor.hpp"

namespace patricia
{

    patricia::serialize_visitor::serialize_visitor(const std::string &filename)
      : filename_(filename)
    { }

    void serialize_visitor::visit(const dynamic_trie *e)
    {
        const std::string& strings = e->get_strings();
        const unsigned long nb_nodes = e->get_nb_nodes();
        std::size_t buffer_size = sizeof (struct static_trie::control);
        buffer_size += strings.length();
        const std::size_t nodes_position = buffer_size;
        buffer_size += nb_nodes * sizeof (struct static_trie::node);
        const std::size_t arcs_position = buffer_size;
        buffer_size += (nb_nodes - 1) * sizeof (struct static_trie::arc);

        int fd = open(filename_.data(), O_CREAT | O_RDWR | O_TRUNC, (mode_t)0644);
        lseek(fd, buffer_size, SEEK_SET);
        if (fd == -1)
        {
            throw std::runtime_error("Could not open file");
        }
        int err_write = write(fd, "", 1);        
        if (err_write == -1 || !err_write) {
            throw std::runtime_error("Could not open file");
        }
        void *mem_block = mmap(nullptr, buffer_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
        if (mem_block == MAP_FAILED)
        {
            throw std::runtime_error("Could not mmap file");
        }
        base_ptr_ = (char*)mem_block;
        fd_ = fd;
        buffer_size_ = buffer_size;

        get_control_block()->strings = sizeof (struct static_trie::control);
        get_control_block()->nodes = nodes_position;
        get_control_block()->arcs = arcs_position;

        strings.copy(base_ptr_ + get_control_block()->strings, strings.length());

        std::queue<const struct dynamic_trie::node*> q;
        q.push(e->get_root());

        unsigned nodes_length = 0;
        while(!q.empty())
        {
            const struct dynamic_trie::node* cur = q.front();
            q.pop();

            symbols_[cur] = nodes_length;
            nodes_length++;

            const auto& map = cur->get_children();
            for (auto& pair : map)
            {
                q.push(pair.first.get_node());
            }
        }

        arc_positions(e->get_root(), 0);
        e->accept(*this);
    }

    unsigned serialize_visitor::arc_positions(const dynamic_trie::node *e, unsigned k)
    {
        get_node_at(symbols_[e])->arcs_index = k;
        auto& map = e->get_children();
        if (!map.size())
        {
            return 0;
        }
        unsigned this_reserved = map.size();
        for (auto pair : map)
        {
            this_reserved += arc_positions(pair.first.get_node(), this_reserved + k);
        }
        return this_reserved;
    }

    void serialize_visitor::visit(const dynamic_trie::node *e)
    {
        static_trie::node* cur = get_node_at(symbols_[e]);

        auto& map = e->get_children();
        cur->arcs_length = map.size();
        cur->value = e->get_value();

        static_trie::arc* a = get_arc_at(cur->arcs_index);
        for (auto pair : map)
        {
            a->key = pair.second;
            const auto *p = pair.first.get_node();
            unsigned val = symbols_[p];
            a->node_index = val;
            a->str_index = pair.first.get_index();
            a->str_length = pair.first.get_length();
            a++;
        }

        e->accept(*this);
    }

    void serialize_visitor::visit(const dynamic_trie::arc *e)
    {
        msync(base_ptr_, buffer_size_, MS_ASYNC);
        e->accept(*this);
    }

    struct static_trie::control *serialize_visitor::get_control_block()
    {
        return (struct static_trie::control*)base_ptr_;
    }

    struct static_trie::arc *serialize_visitor::get_arc_at(unsigned pos)
    {
        void * ptr = base_ptr_ + get_control_block()->arcs;
        static_trie::arc* arc = (static_trie::arc*)ptr + pos;
        return arc;
    }

    struct static_trie::node *serialize_visitor::get_node_at(unsigned pos)
    {
        void * ptr = base_ptr_ + get_control_block()->nodes;
        static_trie::node* node = (static_trie::node*)ptr + pos;
        return node;
    }

    serialize_visitor::~serialize_visitor()
    {
        munmap(base_ptr_, buffer_size_);
        close(fd_);
    }


}
