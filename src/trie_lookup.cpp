# include <fstream>
# include <sys/mman.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <fcntl.h>
# include <dot_visitor.hpp>

# include "serialize_visitor.hpp"
# include "static_trie.hpp"
# include "word_listing_visitor.hpp"

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " [dict.bin] [output.dot]" << std::endl;
        exit(1);
    }
    struct stat sb;
    int fd = open(argv[1], O_RDWR);
    if (fd == -1) {
        std::cerr << "Error opening file" << std::endl;
    }
    fstat(fd, &sb);
    void *mem_block = mmap(nullptr, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (mem_block == MAP_FAILED)
    {
        std::cerr << "Could not mmap the file" << std::endl;
        exit(2);
    }

    {
        patricia::static_trie s_t = patricia::static_trie(mem_block, sb.st_size);

        std::ofstream file;
        file.open(argv[2]);
        patricia::dot_visitor d_v(file, "patricia_trie");
        d_v.visit(&s_t);

        patricia::word_listing_visitor l_v(std::cout);
        l_v.visit(&s_t);
    }

    munmap(mem_block, sb.st_size);
    return 0;

}
