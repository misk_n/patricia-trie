# include "dynamic_trie.hpp"

namespace patricia
{
    void delete_visitor::visit(const patricia::dynamic_trie *)
    {
        throw std::runtime_error("Can not use the destructor dynamic_visitor for a dynamic_trie type.");
    }

    void delete_visitor::visit(const patricia::dynamic_trie::node *e)
    {
        e->accept(*this);
        delete e;
    }

    void delete_visitor::visit(const patricia::dynamic_trie::arc *e)
    {
        e->accept(*this);
    }
}