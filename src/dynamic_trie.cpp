#include <algorithm>
# include "dynamic_trie.hpp"

namespace patricia {

    static unsigned long common_prefix_length(const std::string &a, const std::string &b)
    {
        unsigned long res;
        for (res = 0; res < a.length() && res < b.length(); res++)
        {
            if (a[res] != b[res])
                break;
        }
        return res;
    }

    static inline bool substring_match_at(const std::string& strings, const std::string& substring, long int pos)
    {
        bool res = true;
        const std::size_t len = substring.length();
        for (unsigned long i = 0; res && i < len; i++)
            res = substring[i] == strings[pos + i];
        return res;
    }

    static inline long int find_substring(const std::string& strings, const std::string& substring)
    {
        if (substring.length() == 1)
        {
            char c = substring[0];
            if (c <= 'z' && c >= 'a')
                return c - 'a';
            if (c <= '9' && c >= '0')
                return 26 + c - '0';
        }
        const std::size_t str_len = strings.length();
        const std::size_t sub_len = substring.length();
        if (str_len < sub_len)
            return -1;
        for (unsigned pos = 0; pos < strings.length(); pos++)
        {
            if (substring_match_at(strings, substring, pos))
                return pos;
        }
        return -1;
    }

    dynamic_trie::arc::arc(unsigned index, unsigned length, struct node *node)
            : index(index), length(length), n(node)
    { }

    dynamic_trie::arc::arc()
    { }

    void dynamic_trie::arc::insert(std::string word, dynamic_trie& t, int value)
    {
        word = word.substr(1);
        auto arc_word = t.strings_.substr(index, length);
        auto prefix_len = common_prefix_length(word, arc_word);
        if (prefix_len < arc_word.length())
        {
            auto *a = new node();
            t.nb_nodes_++;
            struct arc to_a(index + prefix_len + 1, length - prefix_len - 1, a);
            length = prefix_len;
            a->map = n->map;
            a->value_ = n->value_;
            n->value_ = -1;
            n->map.clear();
            n->map.emplace_back(to_a, arc_word[prefix_len]);
            n->map.shrink_to_fit();
            //n->map[arc_word[prefix_len]] = to_a;
        }
        n->insert(word.substr(prefix_len), t, value);
    }

    void dynamic_trie::arc::accept(dynamic_visitor &v) const
    {
        v.visit(this->n);
    }

    const dynamic_trie::node *dynamic_trie::arc::get_node() const
    {
        return n;
    }

    unsigned dynamic_trie::arc::get_index() const
    {
        return index;
    }

    unsigned dynamic_trie::arc::get_length() const
    {
        return length;
    }

    inline static bool operator< (const std::pair<dynamic_trie::arc, char>& a, const std::pair<dynamic_trie::arc, char>& b)
    {
        return a.second < b.second;
    }

    void dynamic_trie::node::insert(std::string word, dynamic_trie &t, int value)
    {
        if (word.empty())
        {
            value_ = value;
            return;
        }
        char key = word[0];

        auto range = std::equal_range(map.begin(), map.end(), std::pair<dynamic_trie::arc, char>(arc(), key));
        if (range.first != range.second)
        {
            range.first->first.insert(word, t, value);
            return;
        }
        auto *term = new node();
        t.nb_nodes_++;
        term->value_ = value;

        const std::string sub = word.substr(1, word.size());
        unsigned pos = t.strings_.length();
        const unsigned len = sub.length();

        if (sub.length())
        {
            if (sub.length() <= 2)
            {
                auto search = find_substring(t.strings_, sub);
                if (search < 0)
                    t.strings_.append(sub);
                else
                    pos = search;
            }
            else
                t.strings_.append(sub);
        }

        arc to_term(pos, len, term);
        map.reserve(map.size() + 1);
        map.emplace_back(to_term, key);
        std::sort(map.begin(), map.end());
    }

    void dynamic_trie::node::accept(dynamic_visitor &v) const
    {
        for (const auto &it : map)
            v.visit(&it.first);
    }

    bool dynamic_trie::node::get_terminal() const
    {
        return value_ > 0;
    }

    int dynamic_trie::node::get_value() const
    {
        return value_;
    }

    const std::vector<std::pair<struct dynamic_trie::arc, char>> &dynamic_trie::node::get_children() const
    {
        return map;
    }

    void dynamic_trie::insert(const std::string &word, int value)
    {
        root_.insert(word, *this, value);
    }

    void dynamic_trie::accept(dynamic_visitor &v) const
    {
        v.visit(&root_);
    }
    dynamic_trie::~dynamic_trie()
    {
        patricia::delete_visitor v;
        for (auto a : root_.get_children())
            v.visit(&a.first);
    }

    const std::string &dynamic_trie::get_strings() const
    {
        return strings_;
    }

    unsigned long dynamic_trie::get_nb_nodes() const
    {
        return nb_nodes_;
    }

    const dynamic_trie::node *dynamic_trie::get_root() const
    {
        return &root_;
    }

}
