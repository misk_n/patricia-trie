#include "dot_visitor.hpp"

namespace patricia
{

    dot_visitor::dot_visitor(std::ostream &output, std::string name)
            : output_(output), name_(std::move(name))
    {}

    void dot_visitor::visit(const dynamic_trie *e)
    {
        output_ << "digraph " << name_ << " {" << std::endl;
        e->accept(*this);
        output_ << "}" << std::endl;
    }

    void dot_visitor::visit(const dynamic_trie::node *e)
    {
        current_dynamic_node_ = e;
        generate_id(e);
        auto &m = e->get_children();
        for (auto p : m)
            arc_keys_.push(p.second);
        e->accept(*this);
        while (!dynamic_next_.empty())
        {
            const dynamic_trie::node *n = dynamic_next_.front();
            dynamic_next_.pop();
            current_dynamic_node_ = n;
            n->accept(*this);
        }
    }

    void dot_visitor::visit(const dynamic_trie::arc *e)
    {
        const auto n = e->get_node();
        char k = arc_keys_.front();
        arc_keys_.pop();
        output_ << "    " << generate_id(current_dynamic_node_) << " -> " << generate_id(n)
                << "    [label=\"" << k << "(" << e->get_index() << ", " << e->get_length() << ")\"];" << std::endl;
        dynamic_next_.push(n);
        auto &m = n->get_children();
        for (auto p : m)
            arc_keys_.push(p.second);
    }

    bool dot_visitor::register_id(const void* ptr, std::string& res)
    {
        bool new_symbol = false;

        if (!symbols_.count(ptr))
        {
            symbols_[ptr] = std::to_string(current_);
            new_symbol = true;
            current_++;
        }
        res = symbols_[ptr];
        return new_symbol;
    }

    std::string dot_visitor::generate_id(const static_trie::node *ptr)
    {
        std::string sym;
        bool new_symbol = register_id(ptr, sym);
        if (new_symbol && ptr->get_terminal())
            output_ << "    " << sym << " [color=blue];" << std::endl;
        return sym;
    }

    std::string dot_visitor::generate_id(const dynamic_trie::node *ptr)
    {
        std::string sym;
        bool new_symbol = register_id(ptr, sym);
        if (new_symbol && ptr->get_terminal())
            output_ << "    " << sym << " [color=blue];" << std::endl;
        return sym;
    }

    void dot_visitor::visit(const static_trie *e)
    {
        static_trie_ = e;
        output_ << "digraph " << name_ << " {" << std::endl;
        e->accept(*this);
        output_ << "}" << std::endl;
    }

    void dot_visitor::visit(const static_trie::node *e)
    {
        std::string sym1 = generate_id(e);
        unsigned base = e->arcs_index;
        for (unsigned i = 0; i < e->arcs_length; i++)
        {
            auto *a = get_static_trie()->get_arc_at(base + i);
            output_ << "    " << sym1 << " -> " << generate_id(get_static_trie()->get_node_at(a->node_index))
                    << "    [label=\"" << a->key
                    << "(" << a->str_index<< ", " << (unsigned)a->str_length << ")\"];" << std::endl;
        }
        e->accept(*this);
    }

    void dot_visitor::visit(const static_trie::arc *e)
    {
        e->accept(*this);
    }

}