#include "word_listing_visitor.hpp"

namespace patricia
{

    void word_listing_visitor::visit(const patricia::static_trie *e)
    {
        static_trie_ = e;
        e->accept(*this);
    }

    void word_listing_visitor::visit(const patricia::static_trie::node *e)
    {
        if (e->get_terminal())
        {
            for (char c : word_)
                out_ << c;
            out_ << '\t' << e->get_frequency() << std::endl;
        }
        e->accept(*this);
    }

    void word_listing_visitor::visit(const patricia::static_trie::arc *e)
    {
        word_.push_back(e->key);
        const char* s = static_trie_->get_string_at(e->str_index);
        for (unsigned i = 0; i < e->str_length; i++)
            word_.push_back(s[i]);

        e->accept(*this);

        for (int i = 0; i < e->str_length + 1; i++)
            word_.pop_back();
    }

    word_listing_visitor::word_listing_visitor(std::ostream& o)
     : out_(o)
    { }

}
