# include <iostream>
# include <fstream>
# include <serialize_visitor.hpp>
# include <sys/mman.h>
# include <fcntl.h>
# include <sys/stat.h>
#include <string>
#include <fstream>
#include <iostream>
#include<iterator>
#include <sstream>
#include <algorithm>
# include "dynamic_trie.hpp"
# include "dot_visitor.hpp"
# include "trash_talker.hpp"

inline const std::vector<std::string> split(const std::string& s)
{
    std::istringstream iss(s);
    std::istream_iterator<std::string> beg(iss), end;
    std::vector<std::string> tokens(beg, end);
    return tokens;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " /path/to/compiled/dict.bin" << std::endl;
        exit(1);
    }
    struct stat sb;
    int fd = open(argv[1], O_RDWR);
    if (fd == -1)
    {
        std::cerr << "Error opening file" << std::endl;
    }
    fstat(fd, &sb);
    void *mem_block = mmap(nullptr, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mem_block == MAP_FAILED)
    {
        std::cerr << "Could not mmap the file" << std::endl;
        exit(3);
    }

    {
        patricia::static_trie s_t = patricia::static_trie(mem_block, sb.st_size);

        for (std::string line; std::getline(std::cin, line);)
        {
            if (line.empty())
                break;
            std::vector<std::string> tokens = split(line);
            s_t.search(tokens[2], std::stoi(tokens[1]));
        }

    }

    munmap(mem_block, sb.st_size);
    return 0;
}


