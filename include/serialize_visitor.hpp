#ifndef PROJECT_SERIALIZE_VISITOR_HPP
#define PROJECT_SERIALIZE_VISITOR_HPP

# include <vector>
# include <queue>

# include "dynamic_trie.hpp"
# include "static_trie.hpp"

namespace patricia
{

    class serialize_visitor : public dynamic_visitor
    {
    public:
        serialize_visitor(const std::string& filename);

        void visit(const dynamic_trie *e) override;

        void visit(const dynamic_trie::node *e) override;

        void visit(const dynamic_trie::arc *e) override;

        ~serialize_visitor();

    private:

        struct static_trie::control* get_control_block();
        struct static_trie::arc* get_arc_at(unsigned pos);
        struct static_trie::node* get_node_at(unsigned pos);
        unsigned int arc_positions(const dynamic_trie::node *e, unsigned k);

    private:
        const std::string filename_;
        char *base_ptr_;
        int fd_;
        std::size_t buffer_size_;
        std::unordered_map<const dynamic_trie::node*, unsigned> symbols_;

    };

}

#endif //PROJECT_SERIALIZE_VISITOR_HPP
