#ifndef PROJECT_PATRICIA_SEARCH_RESULT_HPP
#define PROJECT_PATRICIA_SEARCH_RESULT_HPP
#include<string>

///
 /// \struct SearchResult
 /// \brief Struct to match requested output JSON
 ///
 /// SearchResult is a struct that contains, 3 attributes, one for word frequency, one for distance calculate from a look up word,
 /// and one to store the word finded.
 ///
struct SearchResult {
    int value_;
    std::string word_;
    unsigned char dist_;
    SearchResult(int value, std::string word, unsigned char dist) {
        value_ = value;
        word_ = word;
        dist_ = dist;
    }
    void toJson() const {
        std::cout << "{\"word\":\"" << word_ << "\",\"freq\":" << value_ << ",\"distance\":" << (int)dist_ << "}";
    }
    bool operator<(const SearchResult& a) const {
        if (dist_ == a.dist_) {
            if (value_ == a.value_) {
                return word_ < a.word_;
            }
            return value_ > a.value_;
        }
        return dist_ < a.dist_;
    }
};

#endif