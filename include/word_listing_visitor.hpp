# ifndef PROJECT_WORD_LISTING_VISITOR_HPP
# define PROJECT_WORD_LISTING_VISITOR_HPP

# include <vector>
# include <fstream>

# include "static_trie.hpp"

namespace patricia
{

    class word_listing_visitor : public static_visitor
    {
    public:
        explicit word_listing_visitor(std::ostream& o);

        void visit(const patricia::static_trie *e) override;
        void visit(const patricia::static_trie::node *e) override;
        void visit(const patricia::static_trie::arc *e) override;

    private:
        std::vector<char> word_;
        std::ostream& out_;
    };

}

#endif //PROJECT_WORD_LISTING_VISITOR_HPP
