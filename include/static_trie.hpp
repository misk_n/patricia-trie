#ifndef PROJECT_PATRICIA_STATIC_TRIE_HPP
#define PROJECT_PATRICIA_STATIC_TRIE_HPP

#include <string>
#include <iostream>
#include <string>
#include <vector>
#include "dynamic_trie.hpp"
#include "search_result.hpp"
 
/*! \namespace patricia
 * 
 * namespace for project
 */
namespace patricia
{
    using vect_value_t = int;

    class static_visitor;

    class static_trie
    {
    public: // internal types
        struct control
        {
            unsigned strings;     // offset in bytes
            unsigned nodes;       // offset in bytes
            unsigned arcs;        // offset in bytes
        };


        struct __attribute__((__packed__)) node
        {
            unsigned arcs_index : 24;  // node index from control offset
            int value;                 // stored value
            unsigned char arcs_length;          // number of nodes

            void accept(static_visitor& v) const;

            inline bool get_terminal() const
            {
                return this->value > 0;
            }

            inline int get_frequency() const
            {
                return value;
            }

        };

        struct __attribute__((__packed__)) arc
        {
            unsigned str_index  : 24;   // char index from control offset
            unsigned char str_length;  // number of chars
            unsigned node_index : 24;   // node index from control offset
            unsigned char key;
            void accept(static_visitor& v) const;

        };

    public: // public interface
    
        /**
         * @brief Construct a new static trie object
         * 
         * @param base_ptr pointer address of the mmap dict.bin
         * @param buffer_length_ size of the file dict.bin
         */
        explicit static_trie(const void *base_ptr, unsigned buffer_length);

        inline const struct node * get_node_at(unsigned pos) const
        {
            return node_base_ + pos;
        }

        /**
         * @brief Get the arc at index from the array of strings
         * 
         * @param pos Position in offset from the begining of the array
         * @return const arc* 
         */
        inline const arc * get_arc_at(unsigned pos) const
        {
            return arc_base_ + pos;
        }


        /**
         * @brief Get the string at index from the array of strings
         * 
         * @param pos Position in offset from the begining of the array
         * @return const char* 
         */
        inline const char *get_string_at(unsigned pos) const
        {
            return string_base_ + pos;
        }
        
        void dump(std::ostream& o) const;
        
        /**
         * @brief Function to find a word in a dictionary with a maxDist for Damerau-Levenshtein Distance
         * 
         * @param word Word to find in the TRIE
         * @param maxDist 
         * @return  void 
         */
        void search(const std::string& word, int maxDist) const;
        
        /**
         * @brief Function to make approximative search of a word in the dictionnary
         * @details If you specifiy the parameter maxDist to 0, it makes the exact search for the word
         * @param n Node to start recursion
         * @param word Word to find in the trie
         * @param c_s Vector to append letter from each node of the trie
         * @param distCompute Double dimension vector for Damerau-Levenshtein
         * @param maxDist 
         * @param s_r Vector contains all word that matches maxDist
         * @return true 
         * @return false 
         */
        bool searchRecursive(const node* n,
                             const std::string& word,
                             std::vector<char>& c_s,
                             std::vector<vect_value_t *>& distCompute,
                             int maxDist,
                             std::vector<SearchResult>& s_r) const;
        
        /**
         * @brief Method to print the output with JSON Format
         * 
         * @param results Vector of Struct SearchResult needed
         */
        void print_json(const std::vector<SearchResult>& results) const;
        void accept(static_visitor &v) const;
    private: // private attributes
        const void* base_ptr_;
        const struct control* control_block_;
        const struct node* node_base_;
        const struct arc* arc_base_;
        const char* string_base_;
        unsigned buffer_length_;

    };

    class static_visitor
    {
    public:
        virtual void visit(const patricia::static_trie *e) = 0;
        virtual void visit(const patricia::static_trie::node *e) = 0;
        virtual void visit(const patricia::static_trie::arc *e) = 0;

        const static_trie* get_static_trie() const;

    protected:
        const static_trie* static_trie_;
    };

}

#endif //PROJECT_PATRICIA_TRIE_HPP
