#ifndef PROJECT_PATRICIA_DYNAMIC_TRIE_HPP
#define PROJECT_PATRICIA_DYNAMIC_TRIE_HPP

# include <string>
# include <unordered_map>
#include <vector>

namespace patricia
{
    class dynamic_visitor;

    class dynamic_trie
    {
    public: // internal types
        struct node;
        
        /**
         * @class patricia::dynamic_trie::arc
         * @brief Construct a new arc object
         * 
         */
        
        class __attribute__((__packed__)) arc
        {
        public: // public interface
            arc(unsigned index, unsigned length, struct node *node);
            arc();
            void insert(std::string word, dynamic_trie &t, int value);
            void accept(dynamic_visitor& v) const;
            const node *get_node() const;
            unsigned get_index() const;
            unsigned get_length() const;

        private: // private attributes
            unsigned index : 24;
            unsigned char length;
            struct node *n;
        };

        class node
        {
        private: // internal types
            friend class arc;

        public: // public interface
            void insert(std::string word, dynamic_trie &t, int value);
            void accept(dynamic_visitor& v) const;
            const std::vector<std::pair<struct arc, char>>& get_children() const;
            bool get_terminal() const;
            int get_value() const;

        private: // private attributes
            std::vector<std::pair<struct arc, char>> map;
            int value_ = -1;
        };

        const node *get_root() const;

    public: // public interface
        void insert(const std::string &word, int value);
        void accept(dynamic_visitor& v) const;
        const std::string& get_strings() const;
        unsigned long get_nb_nodes() const;
        ~dynamic_trie();

    private: // private attributes
        std::string strings_ = "abcdefghijklmnopqrstuvwxyz0123456789";
        struct node root_;
        unsigned long nb_nodes_ = 1;
    };

    class dynamic_visitor
    {
    public:
        virtual void visit(const patricia::dynamic_trie *e) = 0;
        virtual void visit(const patricia::dynamic_trie::node *e) = 0;
        virtual void visit(const patricia::dynamic_trie::arc *e) = 0;
    };

    class delete_visitor : public dynamic_visitor
    {
    public:
        void visit(const dynamic_trie *e) override;
        void visit(const dynamic_trie::node *e) override;
        void visit(const dynamic_trie::arc *e) override;
    };


}

#endif //PROJECT_PATRICIA_TRIE_HPP
