#ifndef PROJECT_TRASH_TALKER_HPP
#define PROJECT_TRASH_TALKER_HPP

#include <iostream>
#include <cstdlib>
#include <ctime>

class trasher
{

public:
    trasher()
    {
        const unsigned int length = 17;

        static const char *sentences[length] = {
                "I was told the best versions only use a trie.",
                "Come on, you can do it.",
                "Will never scale!",
                "Yo mama's so big, she's bigger than your dict.bin!",
                "This implementation is public, you MUST do better.",
                "JaVA iS fAsT EnOugH...",
                "gO lANgUagE is FoR meMOrY oPTiMiSAtioN...",
                "Beware the almighty cache miss!",
                "Sylvain Utard <3",
                "Algolia <3",
                "You want to go on vacations, right?",
                "...",
                "I don't like you.",
                "You try to count te sentences right?",
                "ASCII table is too mainstream.",
                "Give up, I'll forgive you.",
                "You're a horrible person."
        };

        srand (time(NULL));
        int i = rand() % length;
        std::cerr << sentences[i] << std::endl;
        std::cerr << "    Written by Axel Petit & Nicolas Misk (EPITA SCIA 2019), project under GPLv3" << std::endl;
    }
};

static trasher trash;

#endif //PROJECT_TRASH_TALKER_HPP
