#ifndef PROJECT_DOT_VISITOR_HPP
#define PROJECT_DOT_VISITOR_HPP

# include <ostream>
# include <unordered_map>
# include <queue>

# include "dynamic_trie.hpp"
# include "static_trie.hpp"

namespace patricia
{

    class dot_visitor
     : public dynamic_visitor
     , public static_visitor
    {
    public:
        dot_visitor(std::ostream &output, std::string name);

        void visit(const dynamic_trie *e) override;
        void visit(const dynamic_trie::node *e) override;
        void visit(const dynamic_trie::arc *e) override;

        void visit(const static_trie *e) override;
        void visit(const static_trie::node *e) override;
        void visit(const static_trie::arc *e) override;

    private:
        std::string generate_id(const dynamic_trie::node *ptr);
        std::string generate_id(const static_trie::node *ptr);
        bool register_id(const void* ptr, std::string& res);

    private:
        std::ostream &output_;
        const std::string name_;
        const dynamic_trie::node *current_dynamic_node_;
        const static_trie::node *current_static_node_;
        std::unordered_map<const void*, std::string> symbols_;
        std::queue<const dynamic_trie::node *> dynamic_next_;
        std::queue<const static_trie::node *> static_next_;
        std::queue<char> arc_keys_;
        unsigned current_ = 0;

    };

}

#endif //PROJECT_DOT_VISITOR_HPP
