Textmining Project
==================

Project under GPLv3, for more information, see LICENSE file.

## Quick start

### Answers to the project questions

This part is in french. It will be removed at the end of the examination process of this 
school project.

#### 1. Decrivez les choix de design de votre programme

Nous avons décidé d'implémenter un patricia trie directement, sans passer par une étape 
d'insertion dans un trie classique.

**Deux classes font office de patricia trie** : *static_trie* et *dynamic_trie*.
Le *dynamic_trie* sert pour l'insertion, au **niveau du compiler**. 
Le *static_trie* sert pour répondre au requêtes au **niveau de l'application**.

Différents visitor design pattern ont été implémentés pour ces structures, soit pour le fonctionnement 
général de la solution, soit pour le debug. Ainsi, **l'algorithme de sérialisation a été 
implémenté sous la forme d'un visitor**. Un visitor a été utilisé pour exporter nos structures
arborescentes au format dot par exemple. Le **binaire Lookup** permet d'effectuer la conversion au format dot
d'un dictionnaire sérialisé par notre compilateur, ainsi que de print tous les mots sauvegardés.

Le static_trie est une structure **purement statique, sauvegardée sur disque**. Un simple mmap permet la 
construction d'un static_trie à partir d'un dictionnaire compilé. C'est concrètement ce qui est fait dans 
le binaire TextMiningApp.

#### 2. Listez l’ensemble des tests effectués sur votre programme (en plus des units tests)

Plusieurs tests ont été effectués:
 - valgrind leak check
 - valgrind massif profiling
 - (linux) perf profiling
 - time : check des prérequis du sujet en termes de q/s/c
 - time : comparaison avec la ref fournie
 - diff des outputs json de la ref et de notre application pour les différents niveaux d'approximation.
 - check de la taille des dictionnaires compilés

#### 3. Avez-vous détecté des cas où la correction par distance ne fonctionnait pas (même avec une distance élevée) ?

Non.

#### 4. Quelle est la structure de données que vous avez implémentée dans votre projet, pourquoi ?

Comme expliqué précédemment, nous avons développé un patricia trie. Nous avons donc pour le trie statique
comme pour le trie dynamique une structure d'arc, et une structure de noeud.
Bien qu'il aurait été possible de contenir toutes les informations possibles dans un struct de noeud,
et que cela aurait permis d'optimiser davantage le compilateur en mémoire, nous avons choisi de
garder cette implémentation par souci de simplicité du code. 

Nous avons fait l'usage de bitfields et structures packed afin d'utiliser le moins de mémoire possible.
L'overhead résultant est à peine perseptible.

Les structures statiques ne contiennent pas de pointeurs. Ceux-ci sont émulés par des offsets dans le
fichier compilé.

#### 5. Proposez un réglage automatique de la distance pour un programme qui prend juste une chaîne de caractères en entrée, donner le processus d’évaluation ainsi que les résultats

Il faudrait choisir une fraction de la taille du mot pour distance. Le critère de validité étant
purement humain, je ne vois pas comment on pourrait automatiser ce choix.

#### 6. Comment comptez vous améliorer les performances de votre programme

##### Partie compilateur

L'usage d'un simple trie, avec un seul type de noeuds, optimisé au maximum en mémoire pour passer
la limite des 512MB, puis converti en patricia trie dans le fichier compilé, pourrait sensiblement 
diminuer la taille du fichier compilé. 

L'usage de vecteurs écrits à la main (avec des malloc/realloc) plutôt que des std::vector dans
les noeuds serait une optimisation mémoire évidente, sans incidence sur les performances.

##### Partie application

Un trop grand nombre d'allocations sont effectuées pendant la recherche. 20% des samples effectués
par l'outil perf se font dans malloc. Il serait possible d'effectuer les allocations à l'avance,
quitte à employer une taille maximale d'un mot fixée de manière arbitraire.

L'algorithme que nous avons employé n'est pas 100% optimal. En effet il a été développé de façon
empirique, et ne correspond pas à l'algorithme de recherche approximative dans un patricia-trie.
Des optimisations algorithmiques sont donc à penser.

Nous ne faisons pas l'usage de caching pour les noeuds les plus hauts dans l'arbre. Il se pourrait
qu'il y ait un gain de performance en utilisant une structure mixte en RAM.

Nous pourrions optimiser la recherche exacte (approx 0) en écrivant une fonction dédiée, et en
implémentant un bloom filter.

#### 7. Que manque-t-il à votre correcteur orthographique pour qu’il soit à l’état de l’art ?

Implémenter un String-Btree.

### Compilation

In the project directory:
```
mkdir build; cd build
cmake ..
make
```

### Usage

```
./TextMiningCompiler /path/to/words.txt /path/to/dict.bin
echo "approx 0 test" | ./TextMiningApp /path/to/dict.bin
echo -e "approx 0 test\napprox 2 test" | ./TextMiningApp /path/to/dict.bin
cat test.txt | ./TextMiningApp /path/to/dict.bin
```

### Documentation

This project is documented with doxygen

```
cd build
make doc
firefox docs/html/index.html
```
