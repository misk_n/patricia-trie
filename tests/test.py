#!/usr/bin/python3

import os
from subprocess import PIPE, Popen, call
import datetime

our_bin_path = '../cmake-build-debug'
our_compiler_path = os.path.join(our_bin_path, 'TextMiningCompiler')
our_app_path = os.path.join(our_bin_path, 'TextMiningApp')
ref_compiler_path = './TextMiningCompiler'
ref_app_path = './TextMiningApp'
words_path = './words.txt'


def main():
    call([ref_compiler_path, words_path, './dict.bin'], stderr=None, stdout=None)
    p1 = Popen(["cat", words_path], stdout=PIPE)
    p2 = Popen(["./format.sh", "0"], stdin=p1.stdout, stdout=PIPE)
    p3 = Popen([ref_app_path, './dict.bin'], stdin=p2.stdout, stdout=PIPE)

    p4 = Popen(['cat', words_path], stdout=PIPE)
    p5 = Popen(["./format.sh", "0"], stdin=p4.stdout, stdout=PIPE)
    p6 = Popen([our_app_path, './dict.bin'], stdin=p5.stdout, stdout=PIPE)

    start = datetime.datetime.now()
    t1 = datetime.datetime.now()

    count = 0
    valid = 0
    period_count = 0
    period_valid = 0
    try:
        while True:
            val1 = p3.stdout.readline()
            val2 = p6.stdout.readable()
            if val1 == b'':
                break
            if val1 == val2:
                valid += 1
                period_valid += 1
            count += 1
            period_count += 1
            if count % 100 == 0:
                t2 = datetime.datetime.now()
                stamp = t2 - t1
                print(str(int(period_count / stamp.total_seconds())) + 'q/s/c - ' +
                      str(int((period_valid * 100) / period_count)) + '% valid',
                      end='\r')
                period_count = 0
                period_valid = 0
                t1 = t2

    except KeyboardInterrupt:
        t2 = datetime.datetime.now()
        stamp = t2 - start
        print()
        print("Average processing rate: " + str(int(count / stamp.total_seconds())) + 'q/s/c')
        print("Processed outputs: " + str(count))
        print("Valid outputs: " + str(valid))
        print("Valid percentage: " + str(int((valid * 100) / count)) + '%')

    p1.wait()
    p4.wait()


main()
