#!/usr/bin/python3
import sys
import argparse
import os
from subprocess import PIPE, Popen, call
import json
ref_app_path = './TextMiningApp'
dict_app_path = './dict.bin'
words_path = '../data/words.txt'
def main(args):
    text_file = args.text_file
    arg_dict = args.dict
    print("### Test ###")
    with open(text_file, 'r') as f:
        for line in f.readlines():
            file_my = line.split()[2] + '_my.txt'
            file_ref = line.split()[2] + '_ref.txt'
            f1 = open(file_my, 'w')
            f2 = open(file_ref, 'w')
            
            p1 = Popen(["echo", line], stdout=PIPE)
            p2 = Popen(["../build/TextMiningApp", arg_dict], stderr=PIPE, stdin=p1.stdout, stdout=PIPE)
            p3 = Popen(["echo", line], stdout=PIPE)
            p4 = Popen([ref_app_path, dict_app_path], stderr=PIPE, stdin=p3.stdout, stdout=PIPE)
            
            f1.write(str(p2.stdout.readline()))
            f2.write(str(p4.stdout.readline()))
            
            p5 = Popen(["diff", file_ref, file_my], stdout=PIPE)   
            
            p5.wait()
            
            f1.close()
            f2.close()
            
            out = str(p5.stdout.readline().decode("utf-8"))
            print(line.strip() + ": {}".format(len(out) == 0))
            os.remove(file_my)
            os.remove(file_ref)

def parse_arguments(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument('text_file', type=str,
                        help='The input file for specific test')
    parser.add_argument('dict', type=str,
                        help='The dict built from TextMiningCompiler')
    return parser.parse_args(argv)


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))